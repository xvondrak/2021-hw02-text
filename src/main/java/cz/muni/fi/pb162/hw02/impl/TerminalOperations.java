package cz.muni.fi.pb162.hw02.impl;

import java.util.List;

/**
 * represents terminal operations
 *
 * @author tomas vondrak
 */
public class TerminalOperations {

    /**
     * print lines
     *
     * @param fileLines lines
     */
    public void lines(List<String> fileLines) {
        for (String line : fileLines) {
            System.out.println(line);
        }
    }

    /**
     * count lines
     *
     * @param fileLines lines
     */
    public void count(List<String> fileLines) {
        System.out.println(fileLines.size());
    }

    /**
     * print lines with their sizes
     *
     * @param fileLines lines
     */
    public void sizes(List<String> fileLines) {
        for (String line : fileLines) {
            System.out.println(line.length() + ": " + line);
        }
    }

    /**
     * print the most similar lines + their distance
     *
     * @param fileLines lines
     */
    public void similar(List<String> fileLines) {
        String mostSimilarFst = null;
        String mostSimilarSec = null;
        int mostSimilarCount = Integer.MAX_VALUE;

        for (String fst: fileLines) {
            for (String sec: fileLines) {
                if (fst.equals(sec)) {
                    break;
                }
                int count = countLevenshtein(fst, sec);
                if (count < mostSimilarCount) {
                    mostSimilarFst = fst;
                    mostSimilarSec = sec;
                    mostSimilarCount = count;
                }
            }
        }

        System.out.println("Distance of " + mostSimilarCount);
        System.out.println(mostSimilarSec + " ~= " + mostSimilarFst);
    }

    /**
     * count levenshtein distance
     *
     * @param fst string
     * @param sec string
     * @return distance
     */
    private int countLevenshtein(String fst, String sec) {
        fst = fst.toLowerCase();
        sec = sec.toLowerCase();

        int[] costs = new int[sec.length() + 1];

        for (int j = 0; j < costs.length; j++) {
            costs[j] = j;
        }

        for (int i = 1; i <= fst.length(); i++) {
            costs[0] = i;
            int nw = i - 1;

            for (int j = 1; j <= sec.length(); j++) {
                int cj = Math.min(1 + Math.min(costs[j], costs[j - 1]),
                        fst.charAt(i - 1) == sec.charAt(j - 1) ? nw : nw + 1);

                nw = costs[j];
                costs[j] = cj;
            }
        }
        return costs[sec.length()];
    }
}
