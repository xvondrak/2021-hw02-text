package cz.muni.fi.pb162.hw02.impl;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * represents intermediate operations
 *
 * @author tomas vondrak
 */
public class IntermediateOperations {

    /**
     * return only uniq lines
     *
     * @param fileLines lines
     * @return list of uniq lines
     */
    public LinkedList<String> uniq(List<String> fileLines) {
        Set<String> uniqStrings = new HashSet<>();
        LinkedList<String> resList = new LinkedList<>();

        for (String line: fileLines) {
            if (!uniqStrings.contains(line)) {
                uniqStrings.add(line);
                resList.add(line);
            }
        }

        return resList;
    }

    /**
     * return sorted lines
     *
     * @param fileLines lines
     */
    public void sort(List<String> fileLines) {
        fileLines.sort(null);
    }

    /**
     * return only duplicate lines
     *
     * @param fileLines lines
     * @return list of duplicate lines
     */
    public LinkedList<String> duplicates(List<String> fileLines) {
        Set<String> uniqStrings = new HashSet<>();
        LinkedList<String> resList = new LinkedList<>();

        for (String line: fileLines) {
            if (uniqStrings.contains(line)) {
                resList.add(line);
            }
            uniqStrings.add(line);
        }

        return resList;
    }
}
