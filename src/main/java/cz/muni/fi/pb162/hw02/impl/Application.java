package cz.muni.fi.pb162.hw02.impl;

import com.beust.jcommander.Parameter;
import cz.muni.fi.pb162.hw02.FileLoader;
import cz.muni.fi.pb162.hw02.Messages;
import cz.muni.fi.pb162.hw02.TerminalOperation;
import cz.muni.fi.pb162.hw02.cmd.CommandLine;
import cz.muni.fi.pb162.hw02.cmd.TerminalOperationConverter;

import java.io.IOException;
import java.util.List;

/**
 * Application class represents the command line interface of this application.
 * <p>
 * You are expected to implement  the  {@link Application#run(CommandLine)} method
 *
 * @author jcechace
 */
public class Application {
    @Parameter(converter = TerminalOperationConverter.class)
    private TerminalOperation terminalOperation = TerminalOperation.LINES;

    @Parameter(names = {"-u"})
    private boolean unique;

    @Parameter(names = {"-s"})
    private boolean sort;

    @Parameter(names = {"-d"})
    private boolean duplicates;

    @Parameter(names = {"--file"}, required = true)
    private String filePath;

    @Parameter(names = "--help", help = true)
    private boolean showUsage = false;

    /**
     * Application entry point
     *
     * @param args command line arguments of the application
     */
    public static void main(String[] args) {
        Application app = new Application();
        CommandLine cli = new CommandLine(app);
        cli.parseArguments(args);

        if (app.showUsage) {
            cli.showUsage();
        } else {
            app.run(cli);
        }
    }

    /**
     * Application runtime logic
     *
     * @param cli command line interface
     */
    private void run(CommandLine cli) {
        List<String> fileLines = null;
        FileLoader fileLoader = new FileLoader();

        try {
            fileLines = fileLoader.loadAsLines(this.filePath);
        } catch (IOException ioException) {
            System.err.println(String.format(Messages.IO_ERROR, this.filePath));
        }

        IntermediateOperations intermediateOperations = new IntermediateOperations();

        if (this.unique && this.duplicates) {
            System.err.println(Messages.INVALID_OPTION_COMBINATION);
        }

        if (fileLines != null) {
            if (this.unique) {
                fileLines = intermediateOperations.uniq(fileLines);
            }

            if (this.sort) {
                intermediateOperations.sort(fileLines);
            }

            if (this.duplicates) {
                fileLines = intermediateOperations.duplicates(fileLines);
            }
        }

        TerminalOperations terminalOperations = new TerminalOperations();

        if (fileLines != null) {
            switch (this.terminalOperation) {
                case LINES:
                    terminalOperations.lines(fileLines);
                    break;
                case COUNT:
                    terminalOperations.count(fileLines);
                    break;
                case SIZES:
                    terminalOperations.sizes(fileLines);
                    break;
                case SIMILAR:
                    terminalOperations.similar(fileLines);
                    break;
                default:
                    System.err.println(Messages.INVALID_OPTION_COMBINATION);
                    System.exit(2);
            }
        }
    }
}
